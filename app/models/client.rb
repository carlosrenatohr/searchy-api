class Client < ActiveRecord::Base

	def self.searching (nombre) 
		query = nombre.to_s.downcase
		self.where('LOWER(nombre) LIKE ?', '%'+query+'%')
	end

end
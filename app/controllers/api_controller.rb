class ApiController < ApplicationController
  
  def all
  	@clients = Client.all
    render json: @clients
  end

  def get
    @client = Client.find(params[:item])
    render json: @client
  end

  def filtering
  	@result = Client.searching(params[:nombre])
  	render json: @result
  end

end

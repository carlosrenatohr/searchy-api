Rails.application.routes.draw do
  resources :users

  root 'api#all'
  get 'api', to: 'api#all'
  get 'client/:item', to: 'api#get'
  get 'lookfor/:nombre', to: 'api#filtering'
  
end
